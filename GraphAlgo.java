import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GraphAlgo
{

    public static int []  generateRandomNumbers(int length)
    {
        int[] array = new int[length];
        Random rng = new Random();
        int min = -10000000;
        int max = 10000000;
        //1 226 919
        //10 000 000

        for(int x = 0; x < length; x++ )
        {
            array[x] = rng.nextInt(max - min);
        }
        return array;
    }

    public static void main(String[] args)
    {
        //System.out.println("Hey");
        if(args.length != 0)
        {
            ArrayList<Integer> list;
            //System.out.println("I made it in here");
            int [] sortedArray;
            int len = Integer.parseInt(args[0]);
            int [] array = generateRandomNumbers(len);

            //System.out.println(Arrays.toString(array));
            try {
                FileWriter fileWriter = new FileWriter("ForFun.csv", true);
                PrintWriter outPutFile = new PrintWriter(fileWriter);
                long startTime = System.currentTimeMillis();
                sortedArray = CountingSort.countingSort(array);
                //System.out.println(Arrays.toString(list.toArray()));
                long endTime = System.currentTimeMillis();
                outPutFile.write(len + " " + (endTime - startTime) + "\n");
                outPutFile.close();

            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

        }

    }
}
