import java.util.Arrays;

public class MergeSort
{
    public static int [] merge(int[] leftArray, int[] rightArray)
    {
        int leftSize = leftArray.length;
        int rightSize = rightArray.length;
        int[] mergedArray = new int[leftSize + rightSize];
        int index = 0, left = 0, right = 0;

        while (left < leftSize || right < rightSize) {
            if (left < leftSize && (right >= rightSize || leftArray[left] <= rightArray[right])) {
                mergedArray[index] = leftArray[left];
                left++;
            } else {
                mergedArray[index] = rightArray[right];
                right++;
            }
            index++;
        }

        return mergedArray;
    }

    public static int [] mergeSort(int[] array)
    {

        int mid = array.length/2;
        if(array.length == 1)
            return array;
        else
           return merge(mergeSort(Arrays.copyOfRange(array, 0, mid)) , mergeSort(Arrays.copyOfRange(array, mid, array.length)));

    }

//    public static void main(String[] args)
//    {
//        long startTime = System.currentTimeMillis();
//        int[] array = { 2, -1, 9, 8, 5, -3, 0, 8};
//        mergeSort(array);
//
//        for (int num : array) {
//            System.out.print(num + " ");
//        }
//        System.out.println();
//        long endTime = System.currentTimeMillis();
//        System.out.println(endTime - startTime);
//
//    }

}
